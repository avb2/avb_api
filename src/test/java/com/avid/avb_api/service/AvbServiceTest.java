package com.avid.avb_api.service;

import com.avid.avb_api.dto.AvbContentDTO;
import com.avid.avb_api.dto.DTOMapper;
import com.avid.avb_api.repository.AssetRepository;
import com.avid.avb_api.repository.AvbRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class AvbServiceTest {

    @TestConfiguration
    static class AvbServiceConfig {
        @Bean
        public AvbService avbService(AvbRepository avbRepository,
                                     AssetRepository assetRepository,
                                     ObjectMapper objectMapper,
                                     DTOMapper dtoMapper) {
            return new AvbService(avbRepository, assetRepository, objectMapper, dtoMapper);
        }
    }

    @Autowired
    private AvbService avbService;

    @MockBean
    private AvbRepository avbRepository;

    @MockBean
    private AssetRepository assetRepository;

    @MockBean
    private ObjectMapper objectMapper;

    @MockBean
    private DTOMapper dtoMapper;

    @MockBean
    private Pageable paging;

    @MockBean
    private AvbContentDTO avbContentDTO;

    @Test
    void return_all_results(){
        //given

        //when
        when(avbRepository.findAll(any(Pageable.class))).thenReturn(Page.empty());
        avbService.getAll(null,0,0);
        //then
        verify(avbRepository).findAll(any(Pageable.class));
    }

    @Test
    void return_results_by_given_query(){
        //given
        String query = "query";
        //when
        avbService.getAll(query,0,0);
        //then
        verify(avbRepository).findByPathContaining(any(String.class),any(Pageable.class));
    }
}