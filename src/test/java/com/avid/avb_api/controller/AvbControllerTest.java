package com.avid.avb_api.controller;

import com.avid.avb_api.dto.AvbContentDTO;
import com.avid.avb_api.dto.AvbFolderDTO;
import com.avid.avb_api.dto.ResultDTO;
import com.avid.avb_api.error_handling.ItemNotFoundException;
import com.avid.avb_api.service.AvbService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.logging.Logger;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@WebMvcTest(AvbController.class)
class AvbControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;

    @MockBean
    private AvbService avbService;

    @MockBean
    private Logger logger;

    private ResultDTO resultDTO = new ResultDTO(Arrays.asList(
            new AvbFolderDTO(2, "//test-path/CloudUX1/proj1/proj1 Bin.avb"),
            new AvbFolderDTO(8, "//test-path/CloudUX1/proj1/testBin.avb")));

    private ResultDTO emptyResultDTO = new ResultDTO(Collections.emptyList());

    private AvbContentDTO avbContentDTO = new AvbContentDTO(2, new ArrayList<>());

    @Test
    void return_all_results_with_unlimited_request() throws Exception {
        //given
        when(avbService.getAll(null, 0, null)).thenReturn(resultDTO);
        //when and then
        mockMvc.perform(get("/")
                .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().isOk())
                .andExpect(content().string(mapper.writeValueAsString(resultDTO)))
                .andExpect(jsonPath("$.results", hasSize(2)));
    }

    @Test
    void return_results_by_given_valid_query() throws Exception {
        //given
        String query = "proj1";
        when(avbService.getAll(query, 0, null)).thenReturn(resultDTO);
        //when and then
        mockMvc.perform(get("/")
                .param("query", query)
                .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().isOk())
                .andExpect(content().string(mapper.writeValueAsString(resultDTO)))
                .andExpect(jsonPath("$.results", hasSize(2)));
    }

    @Test
    void return_results_by_given_invalid_query() throws Exception {
        //given
        String query = "unknown";
        when(avbService.getAll(query, 0, null)).thenReturn(emptyResultDTO);
        //when and then
        mockMvc.perform(get("/")
                .param("query", query)
                .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().isOk())
                .andExpect(content().string(mapper.writeValueAsString(emptyResultDTO)))
                .andExpect(jsonPath("$.results", hasSize(0)));
    }

    @Test
    void return_skipped_results() throws Exception {
        //given
        when(avbService.getAll(null, 1, null)).thenReturn(resultDTO);
        //when and then
        mockMvc.perform(get("/")
                .param("skip", "1")
                .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().isOk())
                .andExpect(content().string(mapper.writeValueAsString(resultDTO)))
                .andExpect(jsonPath("$.results", hasSize(2)));
    }

    @Test
    void return_limited_results() throws Exception {
        //given
        when(avbService.getAll(null, 0, 2)).thenReturn(resultDTO);
        //when and then
        mockMvc.perform(get("/")
                .param("limit", "2")
                .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().isOk())
                .andExpect(content().string(mapper.writeValueAsString(resultDTO)))
                .andExpect(jsonPath("$.results", hasSize(2)));
    }

    @Test
    void return_folder_content_by_given_valid_path() throws Exception {
        //given
        String encodedPath = "%2F%2Ftest-path%2FCloudUX1%2Fproj1%2Fproj1%20Bin.avb";
        when(avbService.getByFolderId(encodedPath, null, 0, null)).thenReturn(avbContentDTO);
        //when and then
        mockMvc.perform(get("/" + encodedPath)
                .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().isOk())
                .andExpect(content().string(mapper.writeValueAsString(avbContentDTO)));
    }

    @Test
    void when_given_invalid_path_then_status_404_with_message_should_be_thrown() throws Exception {
        //given
        String encodedPath = "%2F%2Ftest-path";
        when(avbService.getByFolderId(encodedPath, null, 0, null))
                .thenThrow(new ItemNotFoundException("Folder with provided path not found"));
        //when and then
        mockMvc.perform(get("/" + encodedPath)
                .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().is(404))
        .andExpect(content().string("Folder with provided path not found"));
    }
}