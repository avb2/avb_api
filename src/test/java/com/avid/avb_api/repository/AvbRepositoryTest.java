package com.avid.avb_api.repository;

import com.avid.avb_api.entity.Avb;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@DataJpaTest
class AvbRepositoryTest {

    @Autowired
    private AvbRepository avbRepository;

    @Autowired
    TestEntityManager testEntityManager;

    Avb avb = new Avb(2, "//test-path/CloudUX1/proj1/proj1 Bin.avb");
    Avb avb1 = new Avb(8, "//test-path/CloudUX1/proj1/testBin.avb");

    @Test
    void when_no_data_then_return_empty_list() {
        //given

        //when
        List<Avb> avbs = avbRepository.findAll();
        //then
        assertNotNull(avbs);
        assertTrue(avbs.isEmpty());
    }

    @Test
    void given_item_when_is_added_to_db_then_reflect_avb_list_size() {
        //given

        //when
        testEntityManager.persist(avb);
        List<Avb> avbs = avbRepository.findAll();
        //then
        assertNotNull(avbs);
        assertEquals(1, avbs.size());
        assertEquals(Arrays.asList(avb), avbs);
    }

    @Test
    void return_items_when_given_valid_query() {
        //given
        String query = "proj1";
        Pageable paging = new OffsetLimitRequest(0, 1);
        //when
        testEntityManager.persist(avb);
        List<Avb> avbs = avbRepository.findByPathContaining(query, paging);
        //then
        assertNotNull(avbs);
        assertEquals(1, avbs.size());
        assertEquals(Arrays.asList(avb), avbs);
    }

    @Test
    void return_empty_list_when_given_invalid_query() {
        //given
        String query = "unknown";
        Pageable paging = new OffsetLimitRequest(0, 1);
        //when
        testEntityManager.persist(avb);
        List<Avb> avbs = avbRepository.findByPathContaining(query, paging);
        //then
        assertNotNull(avbs);
        assertTrue(avbs.isEmpty());
    }

    @Test
    void return_second_item_of_two_when_are_skipped_by_one() {
        //given
        Pageable paging = new OffsetLimitRequest(1, 2);
        //when
        testEntityManager.persist(avb);
        testEntityManager.persist(avb1);
        List<Avb> avbs = avbRepository.findAll(paging).toList();
        //then
        assertNotNull(avbs);
        assertEquals(1, avbs.size());
        assertEquals(Arrays.asList(avb1), avbs);
    }

    @Test
    void return_one_item_of_two_when_given_limit_is_one() {
        //given
        Pageable paging = new OffsetLimitRequest(0, 1);
        //when
        testEntityManager.persist(avb);
        List<Avb> avbs = avbRepository.findAll(paging).toList();
        //then
        assertNotNull(avbs);
        assertEquals(1, avbs.size());
        assertEquals(Arrays.asList(avb), avbs);
    }
}