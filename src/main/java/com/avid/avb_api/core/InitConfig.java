package com.avid.avb_api.core;

import com.avid.avb_api.entity.Avb;
import com.avid.avb_api.service.AvbService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.logging.Logger;

@Configuration
public class InitConfig {

    private final ObjectMapper mapper;
    private final Logger logger;

    @Autowired
    public InitConfig(ObjectMapper mapper, Logger logger) {
        this.mapper = mapper;
        this.logger = logger;
    }

    @Bean
    CommandLineRunner runner(AvbService avbService, @Value("${project.init.data}") String initData) {
        return args -> {
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            TypeReference<HashMap<String, Avb>> typeReference = new TypeReference<HashMap<String, Avb>>() {
            };
            InputStream inputStream = TypeReference.class.getResourceAsStream(initData);
            try {
                HashMap<String, Avb> avbs = mapper.readValue(inputStream, typeReference);
                avbService.saveAll(avbs);
                logger.info("Data saved");
            } catch (IOException e) {
                logger.severe("Unable to save data: " + e.getMessage());
            }
        };
    }
}
