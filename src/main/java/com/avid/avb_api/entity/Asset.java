package com.avid.avb_api.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.List;

@Entity
public class Asset {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Attribute> attributes;

    @OneToOne(cascade = CascadeType.ALL)
    private Base base;

    @OneToMany(cascade = CascadeType.ALL)
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonProperty("media-items")
    private List<MediaItems> mediaItems;

    @OneToOne(cascade = CascadeType.ALL)
    private Common common;

    private String mobId;

    public Asset() {
    }

    public Asset(List<Attribute> attributes, Base base, List<MediaItems> mediaItems, Common common, String mobId, Avb avb) {
        this.attributes = attributes;
        this.base = base;
        this.mediaItems = mediaItems;
        this.common = common;
        this.mobId = mobId;
        this.avb = avb;
    }

    @ManyToOne
    @JsonBackReference
    private Avb avb;

    public List<Attribute> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<Attribute> attributes) {
        this.attributes = attributes;
    }

    public Base getBase() {
        return base;
    }

    public void setBase(Base base) {
        this.base = base;
    }

    public List<MediaItems> getMediaItems() {
        return mediaItems;
    }

    public void setMediaItems(List<MediaItems> mediaItems) {
        this.mediaItems = mediaItems;
    }

    public Common getCommon() {
        return common;
    }

    public void setCommon(Common common) {
        this.common = common;
    }

    public String getMobId() {
        return mobId;
    }

    public void setMobId(String mobId) {
        this.mobId = mobId;
    }

    public Avb getAvb() {
        return avb;
    }

    public void setAvb(Avb avb) {
        this.avb = avb;
    }
}
