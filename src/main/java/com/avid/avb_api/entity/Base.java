package com.avid.avb_api.entity;

import javax.persistence.*;

@Entity
public class Base {

    @Id
    private String id;

    @Enumerated(EnumType.STRING)
    private Type type;

    public Base() {
    }

    public Base(String id, Type type) {
        this.id = id;
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }
}
