package com.avid.avb_api.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.List;

@Entity
public class Avb {

    private long id;

    @Id
    private String path;

    public Avb() {
    }

    public Avb(long id, String path) {
        this.id = id;
        this.path = path;
    }

    @OneToMany(cascade = CascadeType.ALL)
    @JsonManagedReference
    private List<Asset> assets;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public List<Asset> getAssets() {
        return assets;
    }

    public void setAssets(List<Asset> assets) {
        this.assets = assets;
    }
}
