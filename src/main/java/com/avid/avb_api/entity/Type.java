package com.avid.avb_api.entity;

import com.fasterxml.jackson.annotation.JsonValue;

import java.util.Arrays;

public enum Type {
    FILEMOB("filemob"),
    MASTERCLIP("masterclip"),
    SUBCLIP("subclip"),
    SEQUENCE("sequence"),
    GROUP("group");

    @JsonValue
    private final String displayName;

    Type(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public static boolean isValidType(String type) {
        return Arrays.stream(Type.values())
                .anyMatch(v -> v.getDisplayName().equals(type));
    }

    public static Type getType(String type) {
        return valueOf(type.toUpperCase());
    }
}
