package com.avid.avb_api.repository;

import com.avid.avb_api.entity.Asset;
import com.avid.avb_api.entity.Type;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AssetRepository extends JpaRepository<Asset, Long> {
    List<Asset> findByAvb_Path(String path, Pageable paging);

    List<Asset> findByAvb_PathAndBase_Type(String path, Type type, Pageable paging);
}
