package com.avid.avb_api.repository;

import com.avid.avb_api.entity.Avb;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AvbRepository extends JpaRepository<Avb, Long> {
    List<Avb> findByPathContaining(String path, Pageable paging);
}
