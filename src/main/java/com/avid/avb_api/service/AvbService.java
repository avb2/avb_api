package com.avid.avb_api.service;

import com.avid.avb_api.dto.AvbContentDTO;
import com.avid.avb_api.dto.AvbFolderDTO;
import com.avid.avb_api.dto.DTOMapper;
import com.avid.avb_api.entity.Asset;
import com.avid.avb_api.entity.Type;
import com.avid.avb_api.error_handling.ItemNotFoundException;
import com.avid.avb_api.repository.AssetRepository;
import com.avid.avb_api.repository.OffsetLimitRequest;
import com.avid.avb_api.dto.ResultDTO;
import com.avid.avb_api.entity.Avb;
import com.avid.avb_api.repository.AvbRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class AvbService {

    private final AvbRepository avbRepository;
    private final AssetRepository assetRepository;
    private final ObjectMapper objectMapper;
    private final DTOMapper dtoMapper;

    @Autowired
    public AvbService(AvbRepository avbRepository,
                      AssetRepository assetRepository,
                      ObjectMapper objectMapper,
                      DTOMapper dtoMapper) {
        this.avbRepository = avbRepository;
        this.assetRepository = assetRepository;
        this.objectMapper = objectMapper;
        this.dtoMapper = dtoMapper;
    }

    public List<Avb> saveAll(HashMap<String, Avb> avbs) {
        return avbRepository.saveAll(avbs.entrySet().stream()
                .map(avb -> {
                    String path = avb.getKey();
                    Avb avbWithPath = avb.getValue();
                    avbWithPath.setPath(path);
                    List<Asset> assets = avbWithPath.getAssets();
                    assets.stream()
                            .forEach(asset -> asset.setAvb(avbWithPath));
                    return avbWithPath;
                })
                .collect(Collectors.toList()));
    }

    public ResultDTO getAll(String query, Integer skip, Integer limit) {
        Pageable paging = new OffsetLimitRequest(skip, limit == null ? (int) avbRepository.count() : limit);

        List<Avb> avbs = query != null ? avbRepository.findByPathContaining(query, paging) :
                avbRepository.findAll(paging).toList();
        return new ResultDTO(avbs.stream()
                .map(avb -> objectMapper.convertValue(avb, AvbFolderDTO.class))
                .collect(Collectors.toList()));
    }

    public AvbContentDTO getByFolderId(String id, String type, Integer skip, Integer limit) throws UnsupportedEncodingException {
        String path = URLDecoder.decode(id, StandardCharsets.UTF_8.toString());
        Pageable paging = new OffsetLimitRequest(skip, limit == null ? (int) assetRepository.count() : limit);

        if (type != null && !Type.isValidType(type)) throw new ItemNotFoundException("Provided type not found");

        AvbContentDTO avbContentDTO = dtoMapper.toDTO(type != null ?
                assetRepository.findByAvb_PathAndBase_Type(path, Type.getType(type), paging) :
                assetRepository.findByAvb_Path(path, paging));

        if (type == null && avbContentDTO.getAssets().isEmpty())
            throw new ItemNotFoundException("Folder with provided path not found");
        if (type != null && avbContentDTO.getAssets().isEmpty())
            throw new ItemNotFoundException("Folder with provided path and type not found");

        return avbContentDTO;
    }
}
