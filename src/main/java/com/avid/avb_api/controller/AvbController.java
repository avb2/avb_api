package com.avid.avb_api.controller;

import com.avid.avb_api.dto.AvbContentDTO;
import com.avid.avb_api.service.AvbService;
import com.avid.avb_api.dto.ResultDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.UnsupportedEncodingException;

@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping
@RestController
public class AvbController {

    private final AvbService avbService;

    @Autowired
    public AvbController(AvbService avbService) {
        this.avbService = avbService;
    }

    @GetMapping
    public ResultDTO getAll(@RequestParam(required = false) String query,
                            @RequestParam(defaultValue = "0") Integer skip,
                            @RequestParam(required = false) Integer limit) {
        return avbService.getAll(query, skip, limit);
    }

    @GetMapping("/{folderId}")
    public AvbContentDTO getById(@PathVariable String folderId,
                                 @RequestParam(required = false) String type,
                                 @RequestParam(defaultValue = "0") Integer skip,
                                 @RequestParam(required = false) Integer limit) throws UnsupportedEncodingException {
        return avbService.getByFolderId(folderId, type, skip, limit);
    }
}
