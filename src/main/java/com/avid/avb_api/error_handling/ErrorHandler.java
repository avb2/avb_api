package com.avid.avb_api.error_handling;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.logging.Logger;

@ControllerAdvice
public class ErrorHandler {

    private final Logger logger;

    @Autowired
    public ErrorHandler(Logger logger) {
        this.logger = logger;
    }

    @ExceptionHandler(value = ItemNotFoundException.class)
    public ResponseEntity<Object> exceptionNoFoundData(ItemNotFoundException e) {
        logger.warning(e.getMessage());
        return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
    }
}
