package com.avid.avb_api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AvbApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(AvbApiApplication.class, args);
    }
}
