package com.avid.avb_api.dto;

import com.avid.avb_api.entity.Asset;

import java.util.List;

public class AvbContentDTO {

    private long id;
    private List<Asset> assets;

    public AvbContentDTO(long id, List<Asset> assets) {
        this.id = id;
        this.assets = assets;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public List<Asset> getAssets() {
        return assets;
    }

    public void setAssets(List<Asset> assets) {
        this.assets = assets;
    }
}
