package com.avid.avb_api.dto;

import com.avid.avb_api.entity.Asset;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class DTOMapper {
    public AvbContentDTO toDTO(List<Asset> assets) {
        return new AvbContentDTO(!assets.isEmpty() ?
                assets.get(0).getAvb().getId() : 0, assets);
    }
}
