package com.avid.avb_api.dto;

import java.util.List;

public class ResultDTO {
    private List<AvbFolderDTO> results;

    public ResultDTO(List<AvbFolderDTO> results) {
        this.results = results;
    }

    public List<AvbFolderDTO> getResults() {
        return results;
    }

    public void setResults(List<AvbFolderDTO> results) {
        this.results = results;
    }
}
